package com.wang.material_rating_bar;

import com.wang.material_rating_bar.util.ElementColorUtil;
import com.wang.material_rating_bar.util.TypedAttrUtils;
import ohos.agp.components.AttrSet;
import ohos.agp.components.Rating;
import ohos.agp.components.element.Element;
import ohos.agp.components.element.VectorElement;
import ohos.app.Context;

public class MaterialRatingBar extends Rating {

    //步进
    private float starStep;

    //默认选中步进
    private float mRating;


    //默认的星星样式
    private VectorElement mDefaultStar;

    //填充后的星星样式
    private VectorElement mDefaultFillStar;

    //填充半星样式
    private VectorElement mHalfFillStar;

    //填充的默认背景
    private Element mDefaultFillDrawable;

    //无填充的默认背景
    private Element mDefaultDrawable;

    //填充半星的默认背景
    private Element mHalfFillDrawable;

    //星星默认颜色
    private int mStarColor;

    //默认背景颜色
    private int mBgColor;

    private int mHalfColor;

    //宽高比
    private float per;

    public MaterialRatingBar(Context context) {
        this(context, null);
    }

    public MaterialRatingBar(Context context, AttrSet attrSet) {
        this(context, attrSet, null);
    }

    public MaterialRatingBar(Context context, AttrSet attrSet, String styleName) {
        super(context, attrSet, styleName);
        mDefaultStar = new VectorElement(context, ResourceTable.Graphic_icon_star_bg_stroke);
        mDefaultStar.setAntiAlias(true);
        mDefaultFillStar = new VectorElement(context, ResourceTable.Graphic_icon_star_fill_stroke);
        mDefaultFillStar.setAntiAlias(true);
        mHalfFillStar = new VectorElement(context, ResourceTable.Graphic_icon_star_half_stroke);
        mHalfFillStar.setAntiAlias(true);

        mDefaultDrawable = TypedAttrUtils.getElement(attrSet, "starElement", mDefaultStar);
        mDefaultFillDrawable = TypedAttrUtils.getElement(attrSet, "starFillElement", mDefaultFillStar);
        mHalfFillDrawable = TypedAttrUtils.getElement(attrSet, "starHalfFillElement", mHalfFillStar);
        starStep = TypedAttrUtils.getFloat(attrSet, "starStep", 1);
        mRating = TypedAttrUtils.getFloat(attrSet, "rating", 0);

        mStarColor = TypedAttrUtils.getIntColor(attrSet, "starColor", 0);
        mBgColor = TypedAttrUtils.getIntColor(attrSet, "bgColor", 0);
        mHalfColor = TypedAttrUtils.getIntColor(attrSet, "halfColor", 0);
        if (mStarColor != 0&&mBgColor != 0) {
            mDefaultDrawable = ElementColorUtil.getTintElement(mDefaultDrawable, mStarColor);
            mDefaultFillDrawable = ElementColorUtil.getTintElement(mDefaultFillDrawable, mBgColor);
            mHalfFillDrawable = ElementColorUtil.getTintElement(mHalfFillDrawable,mBgColor);
        }

        setGrainSize(starStep);
        per = mDefaultDrawable.getWidth() / mDefaultDrawable.getHeight();

        int height = getHeight();
        int width = (int) (height * per);
        mDefaultDrawable.setBounds(0, 0, width, height);
        mDefaultFillDrawable.setBounds(0, 0, width, height);
        mHalfFillDrawable.setBounds(0, 0, width, height);

        setFilledElement(mDefaultFillDrawable);
        setUnfilledElement(mDefaultDrawable);
        setHalfFilledElement(mHalfFillDrawable);
        setRating(mRating);
    }

    public void setRating(float rating) {
        setScore(rating);
    }
}

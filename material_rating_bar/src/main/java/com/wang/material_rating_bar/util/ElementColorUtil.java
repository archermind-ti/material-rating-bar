/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.wang.material_rating_bar.util;

import ohos.agp.components.element.Element;
import ohos.agp.render.BlendMode;

/**
 * ElementColorUtil工具列
 *
 * @since 2021-04-13
 */
public class ElementColorUtil {
    private static final int SIZE = 2;
    private static final int COUNT = -1;

    /**
     * 修改Element颜色
     *
     * @param element 元素
     * @param intColor 颜色值
     * @return Element 改色后元素
     */
    public static Element getTintElement(Element element, int intColor) {
        int[] colors = new int[]{intColor, intColor};
        int[][] states = new int[SIZE][];
        states[0] = new int[]{1};
        states[1] = new int[]{COUNT};
        element.setStateColorList(states, colors);
        element.setStateColorMode(BlendMode.SRC_ATOP);
        return element;
    }
}

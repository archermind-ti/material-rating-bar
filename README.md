# MaterialRatingBar-ohos

### 简介
提供星型打分条样式

### 功能
1. 提供星型打分条样式

### 演示
![输入图片说明](demo.gif)

### 集成

方式一

1. 在moudle级别下的build.gradle文件中添加依赖，在dependences标签中增加对lib包的引用。
```
dependencies {
    implementation fileTree(dir: 'libs', include: ['*.jar'])
    implementation project(':material_rating_bar')
    testImplementation 'junit:junit:4.13'
}
```


方式二

在project的build.gradle中添加mavenCentral()的引用

```
 repositories {   
 	...   
 	mavenCentral()   
	...           
 }
```

在entry的build.gradle中添加依赖


```
 dependencies { 
        ... 
        implementation 'com.gitee.archermind-ti:material_rating_bar:1.0.0' 
        ... 
 }
```

### 使用说明

1.xml中直接使用

```
    <com.wang.material_rating_bar.MaterialRatingBar
        ohos:height="48vp"
        ohos:width="match_content"
        ohos:start_margin="10vp"
        ohos:top_margin="10vp"
        app:rating="3.5"
        app:starElement="$graphic:icon_star_bg_solid"
        app:starFillElement="$graphic:icon_star_fill_solid"
        app:starHalfFillElement="$graphic:icon_star_half_solid"
        app:starStep="0.5"/>
```
2.代码中使用修改星星的样式


```
        ratingBar.setUnfilledElement(getElement(ResourceTable.Graphic_icon_star_bg_stroke));
        ratingBar.setFilledElement(getElement(ResourceTable.Graphic_icon_star_fill_stroke));
        ratingBar.setHalfFilledElement(getElement(ResourceTable.Graphic_icon_star_half_stroke));

        private VectorElement getElement(int resourceId) {
             VectorElement element = new VectorElement(getApplicationContext(), resourceId);
             element.setAntiAlias(true);
             return element;
        }
```


### 编译说明
1. 将项目通过git clone 至本地
2. 使用DevEco Studio 打开该项目，然后等待Gradle 构建完成
3. 点击Run运行即可（真机运行可能需要配置签名）

### 版本迭代
v1.0 初始版本 [changelog](https://gitee.com/archermind-ti/material-rating-bar-ohos/blob/dev/changelog.md)


### 版权和许可信息
- Apache Lisence 2.0
Copyright 2016 Zhang Hai

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.


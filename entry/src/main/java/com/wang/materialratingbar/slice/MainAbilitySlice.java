package com.wang.materialratingbar.slice;

import com.wang.materialratingbar.ResourceTable;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Rating;
import ohos.agp.components.element.VectorElement;

public class MainAbilitySlice extends AbilitySlice {

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_main);

        Rating normalRating1 = (Rating) findComponentById(ResourceTable.Id_normal_rating1);

        VectorElement mDefaultStar = new VectorElement(getApplicationContext(),
                ResourceTable.Graphic_ic_rating_star_border);
        mDefaultStar.setAntiAlias(true);
        VectorElement mDefaultFillStar = new VectorElement(getApplicationContext(),
                ResourceTable.Graphic_ic_rating_star_solid);
        mDefaultFillStar.setAntiAlias(true);
        VectorElement mHalfFillStar = new VectorElement(getApplicationContext(), ResourceTable.Graphic_ic_rating_star_half);
        mHalfFillStar.setAntiAlias(true);

        normalRating1.setUnfilledElement(getElement(ResourceTable.Graphic_ic_rating_star_border));
        normalRating1.setFilledElement(getElement(ResourceTable.Graphic_ic_rating_star_solid));
        normalRating1.setHalfFilledElement(getElement(ResourceTable.Graphic_icon_star_half_stroke));
    }

    private VectorElement getElement(int resourceId) {
        VectorElement element = new VectorElement(getApplicationContext(), resourceId);
        element.setAntiAlias(true);
        return element;
    }


    @Override
    public void onActive() {
        super.onActive();
    }

    @Override
    public void onForeground(Intent intent) {
        super.onForeground(intent);
    }
}
